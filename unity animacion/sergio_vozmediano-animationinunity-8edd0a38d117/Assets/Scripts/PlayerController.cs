﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    private Rigidbody rigidBody;        // Player's rigidbody
    private Animator animator;          // Player's animator
    private float hInput;               // horizontal input
    private float vInput;               // vertical input
    private float runInput;             // Run buttom
    private float currentSpeed = 0;     // Current speed of the player
    public float walkSpeed = 3;         // Player speed when walking
    public float runSpeed = 6;          // Player speed when running 
    public float turnSmoothing = 20f;   // Turn speed

    public float idleWaitingTime = 5f;  // Time to wait to try waiting animation in idle animator state
    private float currentWaitingTime = 0;   // Current time that player already waits


    public Rigidbody rb;
    public float fuerzaDeSalto = 8f;
    public bool puedoSaltar;



    // Use this for initialization
    void Start() {

        rigidBody = GetComponent<Rigidbody>();

        if (rigidBody == null) {
            Debug.Log("No rigidbody component found in this gameobject");
            enabled = false;
            return;
        }

        animator = GetComponent<Animator>();

        if (animator == null) {
            Debug.Log("No animator component found in this gameobject");
            enabled = false;
            return;
        }

        puedoSaltar = false;
	}

	
	// Update is called once per frame
	void Update () {

        // Input
        hInput = Input.GetAxis("Horizontal");
        vInput = Input.GetAxis("Vertical");
        runInput = Input.GetAxis("Run");

        handleRotation();

        // Increase waiting time only when player speed is zero.
        if (currentSpeed == 0) {
            currentWaitingTime += Time.deltaTime;
        }
        else {
            currentWaitingTime = 0;
        }

        if (puedoSaltar)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                animator.SetBool("salte", true);
                rb.AddForce(new Vector3(0, fuerzaDeSalto, 0), ForceMode.Impulse);
            }
            animator.SetBool("tocoSuelo", true);
        }
        else
        {
            estoyCayendo();
        }

    }

    public void estoyCayendo()
    {
        animator.SetBool("tocoSuelo", false);
        animator.SetBool("salte", false);
    }

    private void OnAnimatorMove() {
        animator.SetFloat("Speed", currentSpeed, currentSpeed * Time.deltaTime, Time.deltaTime);

        if (currentWaitingTime >= idleWaitingTime) {
            if (Random.Range(0, 99) < 30) {
                animator.SetTrigger("OnWait");
            }

            currentWaitingTime = 0;
        }
        
    }


    private void FixedUpdate() {
        handleMovement();
    }

    private void handleMovement() {
        // Walkspeed
        float targetSpeed = walkSpeed;

        if (runInput > 0)
            targetSpeed = runSpeed;

        // If we got input from horizontal or vertical axis
        if (hInput != 0 || vInput != 0) {
            rigidBody.velocity = new Vector3(hInput, 0, vInput) * targetSpeed;
        }
        else {
            rigidBody.velocity = Vector3.zero; // No input, stop the player
        }


        currentSpeed = rigidBody.velocity.magnitude; // velocity to speed conversion 

    }

    private void handleRotation() {
        // Rotation
        if (hInput != 0 || vInput != 0) {
            Quaternion newRotation = Quaternion.LookRotation(new Vector3(hInput, 0, vInput));
            transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, Time.deltaTime * turnSmoothing); // How fast the player turns from one direction to another
        }
    }
}
